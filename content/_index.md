---
title: "Home"
draft: false
---

### My Projects
---
- Pak: A cross-platform wrapper written in go designed to unify package managers. It uses TOML configs to define package managers. Options include commands, shortcuts, root user invocation, root user invocation command, and package manager command.

{{< button-gitea project="pak" text="Pak" color="green" >}}
{{< button-gitlab project="pak" text="Pak" color="OrangeRed" >}}

- Opensend: A program made to share files and websites between computers securely and reliably, written in go, using zeroconf for discovery, ChaCha20-Poly1305 and RSA for encryption, and Tar + Zstandard for compression and archiving.

{{< button-gitea project="opensend" text="Opensend" owner="opensend" color="green" >}}
{{< button-gitlab project="opensend" text="Opensend"  color="OrangeRed" >}}

- Simpledash: A full-stack web application to act as a dashboard for important links and information. It is written mostly in go, other than the SQLite driver for session storage. It is configured via a TOML file called `simpledash.toml`. Passwords are stored as bcrypt hashes.

{{< button-gitea project="simpledash" text="Simpledash" color="green" >}}
{{< button-gitlab project="simpledash" text="Simpledash" color="OrangeRed" >}}

- Chromebook Linux Audio: A collection of bash scripts to compile and install the required kernel and audio server to enable audio and other chromebook features in a mainline linux distro.

{{< button-gitlab project="chromebook-linux-audio" text="Chromebook Linux Audio" color="OrangeRed" >}}
