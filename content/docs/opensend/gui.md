---
title: "GUI"
draft: false
description: "Opensend fyne GUI"
---

{{< appveyor-ci project="opensend" projectID="wrv3bbuujw57578h" >}}
{{< minio-s3 project="opensend" >}}

This page assumes you have already installed Opensend. If not, follow the installation
instructions on the installation page.

{{< button text="Installation" link="../installation" color="blue">}}

### GUI Installation

Opensend GUI has been written in golang using [fyne](https://fyne.io). Its source code can be found here:

{{< button-gitea text="Opensend GUI" project="opensend-gui" owner="opensend" color="green" >}}

To download Opensend GUI, run the following command

```bash
git clone https://gitea.arsenm.dev/opensend/opensend-gui.git
```

To build Opensend GUI, `go` must be installed. The process for that is explained in the installation instructions for Opensend. Once `go` is installed, run:

```bash
go build
```

This may take a while as `go` downloads and compiles Opensend GUI and Fyne.

Once the build is complete, there should be a file named `opensend-gui` in the directory. Run this file to open the GUI which should look like this:

{{< image src="/opensend/gui_start.webp" alt="Opensend GUI on start" >}}