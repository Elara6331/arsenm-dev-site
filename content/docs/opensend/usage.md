---
title: "Usage"
draft: false
description: "Using opensend"
---
{{< appveyor-ci project="opensend" projectID="wrv3bbuujw57578h" >}}
{{< minio-s3 project="opensend" >}}

This page assumes you have already installed Opensend. If not, follow the installation
instructions on the installation page.

{{< button text="Installation" link="../installation" color="blue">}}

### Configuration
Opensend allows configuration by TOML and by command line flags. It looks at the following paths for configs in the specified order:

#### Config files

1. Config path from `--config` flag
2. `~/.config/opensend.toml`
3. `/etc/opensend.toml`

#### Command line flags

```text
Usage of opensend:
  -d string
        Data to send
  -dest-dir string
        Destination directory for files or dirs sent over opensend (default "/home/arsen/Downloads")
  -r    Receive data
  -s    Send data
  -send-to string
        Use IP address of receiver instead of mDNS
  -skip-mdns
        Skip zeroconf service registration (use if mdns fails)
  -t string
        Type of data being sent
```

The purpose of the mdns-skipping flags is to account for the iSH app in iOS, as the mdns resolver and registration fails on it.

### Algorithms and software used

- RSA for asymmetric encryption
- AES for symmetric encryption
- Tar for archiving directories
- Zstandard for compression
- Base91 for encoding
- Gob for serialization
- JSON for serialization
- TCP sockets for transfer
- Zeroconf/mDNS for device discovery