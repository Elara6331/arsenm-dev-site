---
title: "Installation"
draft: false
description: "Installing opensend"
---
{{< appveyor-ci project="opensend" projectID="wrv3bbuujw57578h" >}}
{{< minio-s3 project="opensend" >}}

## Using precompiled binary
Opensend uses continuous integration to compile. You can find the binary by clicking the download binary badge above.

## Building from source

### Downloading
Opensend is hosted on Gitea.

{{< button-gitea color="green" project="opensend" owner="opensend" text="Opensend" >}}
{{< button-gitlab project="opensend" text="Opensend"  color="OrangeRed" >}}

To download opensend, you can either use the download button on one of the above, or
you can use the git command

To clone opensend using the command, run the following command:
```bash
git clone https://gitea.arsenm.dev/opensend/opensend.git
```

Now, you will want to `cd` into the root of this repo before completing the rest
of these instructions

### Building

Since Opensend is written in go, you will need go installed in order to compile it.
Most linux distros call the package providing it either `go` or `golang`.

Once go is installed, you can check that it runs by running
```bash
go version
```
To compile Opensend, run the following command:
```bash
make
```

### Installing

To install opensend, run one of the following commands:

```bash
sudo make install # Linux
sudo make install-macos # macOS
```

Once this command completes, to test whether opensend was installed properly, run
this command:

```bash
opensend -h
```

You should get the usage for opensend.
