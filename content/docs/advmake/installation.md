---
title: "Installation"
draft: false
description: "Installing AdvMake"
---
{{< button-gitea color="green" project="advmake" text="AdvMake" >}}
{{< button-gitlab color="OrangeRed" project="advmake" text="AdvMake" >}}


## Building from source

### Downloading
AdvMake is hosted on my Gitea instance. If that is down, it is also mirrored on Gitlab.

To download AdvMake, you can either use the download button on Gitea or Gitlab, or
you can use the git CLI

To clone AdvMake using the CLI, run one of the following commands:
```bash
git clone https://gitea.arsenm.dev/Arsen6331/advmake.git
OR
git clone https://gitlab.com/moussaelianarsen/advmake.git
```

### Building

AdvMake is written in Go. This means go must be installed on your computer. Most
linux distros call the package that provides it either `go` or `golang`.

Once go is installed, you can check that it runs by running
```bash
go version
```
To compile AdvMake, run
```bash
go build
```

### Installing

To install AdvMake, run:
```bash
sudo install -Dm755 advmake /usr/bin
```

Once the command completes, AdvMake should be ready and you can run the following to make sure it works:

```bash
advmake -h
```