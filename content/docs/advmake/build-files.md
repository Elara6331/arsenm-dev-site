---
title: "Build Files"
draft: false
description: "Understanding AdvMake Build Files"
---
{{< button-gitea color="green" project="advmake" text="AdvMake" >}}
{{< button-gitlab color="OrangeRed" project="advmake" text="AdvMake" >}}


## Format
AdvMake uses [Starlark](https://github.com/bazelbuild/starlark) as the format for its build files.
Modules are also defined for both convenience and extra functionality.

Starlark is a Python-like language meant for configuration files.

## Configuration
Build files are by default called `AdvMakefile`, but that can be set via `-f`

An AdvMakefile example can be found at AdvMake's repo as it uses AdvMake itself.

AdvMake runs functions exposed by starlark in the format `<name>_<target>`.
To set the default name and target, the global variables `defaultName`, and `defaultTarget` must be set.
Here is an example from AdvMake's AdvMakefile:
```python
defaultName = "advmake"
defaultTarget = "build"
```
This will tell AdvMake to run the function `advmake_build()` when run with no arguments.

If AdvMake is run with one argument (such as `advmake install`), it will use the default name with the specified target,
so in that case, it would run `advmake_install()`. 

If run with two arguments, AdvMake will use the first argument as the name and the second as the target.
So, running `advmake hello world` would run the function `hello_world()`.

## Modules
As previously mentioned, AdvMake comes with modules. Those are as follows:

### `runtime`

The runtime module exposes some of golang's runtime methods and variables.

---

#### `runtime.GOOS`

Stores a string denoting the operating system being used.

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/runtime#GOOS" >}}

---

#### `runtime.GOARCH`

Stores a string denoting the CPU architecture being used.

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/runtime#GOARCH" >}}

---

#### `runtime.NumCPU()`

Get the number of logical CPUs available to the current process

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/runtime#NumCPU" >}}

---

#### `runtime.GOMAXPROCS()`

Definition: `runtime.GOMAXPROCS(n)`

Get or set the value of the GOMAXPROCS environment variable. This variable controls the maximum number of CPUs that can execute. This function will set GOMAXPROCS to n and then return the previous value. If `n<1`, this function will not set the variable and will instead return the current setting

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/runtime#GOMAXPROCS" >}}


---

### `encoding`

The strings module contains functions for encoding and decoding various formats. This module contains submodules for the various formats

Available submodules:

- `Json`
- `Yaml`
- `Toml`
- `Hex`

---

#### `encoding.<Submodule>.Load()`

Load a string formatted as the submodule format into a dictionary or string.

Examples:
```python
x = encoding.Json.Load('{"encoding": "json"}')
# x["encoding"] == "json"
y = encoding.Hex.Load('546573740a')
# y == "Test"
```

---

#### `encoding.<Submodule>.Dump()`

Dump a string formatted as the submodule format from a dictionary or string

Examples:
```python
xDict = {"encoding": {"type": "toml"}}
x = encoding.Toml.Dump(xDict)
# x == '''
#
# [encoding]
#   type = "toml"
#
# '''
y = encoding.Hex.Dump("Test")
# y = "546573740a"
```

---

### `file`

The file module contains functions for manipulation and checking of files

---

#### `file.Expand()`

Definition: `file.Expand(file, mappings)`

Expand any instances of `$VAR` in a file according to provided mappings.

Examples:

`file.txt` before:
```text
I am running on $OS and architecture $arch
```

Code:
```python
file.Expand("file.txt", {"OS": runtime.GOOS, "arch": runtime.GOARCH})
```

`file.txt` after:

```text
I am running on linux and architecture x86_64
```

---

#### `file.Exists()`

Definition: `file.Exists(filepath)`

Check whether a file exists

Example:
```python
file.Exists("/etc/fstab") # True
```

---

#### `file.Content()`

Definition: `file.Content(filepath)`

Returns contents of a file as a string

Example:

file.txt:
```text
This is a file
```

Code:
```python
file.Content("file.txt") # "This is a file"
```

---

### `strings`

The strings module contains functions for the manipulation of strings

---

#### `strings.Regex()`

Definition: `strings.Regex(string, pattern, regex)`

Parse a string using a regular expression and return the result in the specified format.

Examples:
```python
x = strings.Regex("Hello, World", "$2, $1", "(.+), (.+)") 
# x == "World, Hello"
y = strings.Regex("Hello, World", "$y, $x", "(?P<x>.+), (?P<y>.+)")
# y == "World, Hello"
z = strings.Regex("Hello, World", "$match, $2, $1", "(.+), (.+)") 
# z == "Hello, World, World, Hello"
```

---

#### `strings.HasSuffix()`

Definition: `strings.HasSuffix(string, suffix)`

Check whether a string ends with a suffix.

Examples:
```python
strings.HasSuffix("doc.pdf", ".pdf") # True
strings.HasSuffix("doc.pdf", ".md") # False
```

---

#### `strings.HasPrefix()`

Definition: `strings.HasPrefix(string, prefix)`

Check whether a string starts with a prefix.

Example:
```python
strings.HasPrefix("doc.pdf", "doc") # True
```

---

#### `strings.TrimSuffix()`

Definition: `strings.HasSuffix(string, suffix)`

Remove suffix from string if it exists. If it does not exist, the string is returned unchanged.

Example:
```python
strings.TrimSuffix("doc.pdf", ".pdf") # "doc"
```

---

#### `strings.TrimPrefix()`

Definition: `strings.TrimPrefix(string, prefix)`

Remove prefix from string if it exists. If it does not exist, the string is returned unchanged.

Example:
```python
strings.TrimPrefix("doc.pdf", "doc") # ".pdf"
```

---

#### `strings.TrimSpace()`

Definition: `strings.TrimSpace(string)`

Trim leading and trailing white space, as defined by Unicode

Example:
```python
strings.TrimSpace("    Hi  ") # "Hi"
```

---

### `input`

The input module prompts the user for input

---

#### `input.Prompt()`

Definition: `input.Prompt(prompt)`

Print prompt and wait for input, returning on newline

Example:
```python
input.Prompt("Enter number: ")
```

---

#### `input.Choice()`

Definition: `input.Choice(prompt, choices)`

Assign number to each choice and prompt user to choose one

Example:
```python
input.Choice("Choose greeting", ["Hi", "Hello", "Good morning"])
```
The above example looks like this to the user:
```text
[1] "Hi"
[2] "Hello"
[3] "Good Morning"
Choose greeting: 
```

When the user chooses a number, the function will return the associated string. So, if the user chooses 1, `"Hi"` will be returned.

---

### `url`

The url module contains functions for the manipulation of URLs

---

#### `url.Parse()`

Definition: `url.Parse(urlString)`

Parses a URL and returns its components

Example:
```python
parsed = url.Parse("https://www.arsenm.dev/docs/advmake/build-files")
# parsed.Scheme == "https"
# parsed.Host == "www.arsenm.dev"
# parsed.Path == "/docs/advmake/build-files"
```

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/net/url#URL" >}}

---

### `shell`

The shell module contains functions for accessing and utilizing the shell.

---

#### `shell.Exec()`

Definition: `shell.Exec(command, output?, concurrent?)`

Runs a command or script using `sh -c`, sending the output to `STDOUT` and returning it unless set otherwise. It can also be concurrent.

Examples:

Code:
```python
x = shell.Exec("date +%r") # "12:00:00 AM"
y = shell.Exec("date +%r", output='return') # "12:00:00 AM"
z = shell.Exec("date +%r | base64", output='stdout') # None
shell.Exec("""
	sleep 1
	sleep 2
""", concurrent=True) # Sleeps for two seconds
```

STDOUT:
```text
12:00:00 AM
MTI6MDA6MDAgQU0K
```

---

#### `shell.Getenv()`

Definition: `shell.Getenv(key)`

Returns the value of an environment variable

Example:
```python
shell.Getenv('TERM') # "xterm"
```

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/os#Getenv" >}}

---

#### `shell.Setenv()`

Definition: `shell.Setenv(key, value, onlyIfUnset?)`

Sets the value of an environment variable. It can be configured not to set the value if it is already set

Examples:
```python
shell.Setenv("X", "x")  # $X = x
shell.Setenv("CC", "gcc") # if $CC unset, $CC = gcc
```

---

#### `shell.LookPath()`

Definition: `shell.LookPath(command)`

Returns the path to the executable of the specified command. Returns `-1` if the command is not found in `PATH`.

Examples:
```python
shell.LookPath('sh') # "/bin/sh"
shell.LookPath('nonExistentCommand') # -1
```

---

### `net`

The net module contains various network functions

---

#### `net.Download()`

Download a file from a URL, optionally specifying the filename. It will show progress if the `Content-Length` header is present.

Examples:
```python
net.Download("https://minio.arsenm.dev/advmake/0.0.1/advmake-linux-x86_64")
net.Download("https://minio.arsenm.dev/advmake/0.0.1/advmake-linux-x86_64", filename="advmake")
```

---

### `log`

The log module contains functions to log events at various levels

The available levels are:

- `Info`
- `Debug`
- `Warn`
- `Fatal`

---

#### `log.<Level>()`

Definition: `log.<Level>(message)`

Logs a message at the specified level. The fatal level quits after logging the message.

Examples:
```python
log.Info("Test log")
log.Fatal("Error")
```

---

### `fmt`

The fmt module exposes all the text functions from the golang fmt package except for all the `Fprint` and `Fscan` functions.

```python
fmt.Sprintf("Print %s string", "formatted") # "Print formatted string"
```

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://pkg.go.dev/fmt" >}}