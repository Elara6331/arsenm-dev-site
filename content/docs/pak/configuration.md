---
title: "Configuration"
draft: false
description: "Configuring pak"
---
{{< appveyor-ci project="pak" projectID="e4yacqd78gkte8a0" >}}
{{< minio-s3 project="pak" >}}

### Config file
Pak uses a custom config file at `/etc/pak.cfg`. For example, this is what the
apt config looks like:

```cfg
# Write the name of the package manager in all lowercase below
apt
# Write a comma separated list of commands from the manager below
install,remove,update,upgrade,search,download
# Write "yes" or "no" depending on whether you want to use root
yes
# Write command to use for root
sudo
# Write a comma separated list of shortcuts below
rm,inst
# Write a comma separated list of shortcut mappings from the manager below
remove,install

```
This file is read by pak to tell it what to do. The comments above each keyword
explain what it's for.

Here is a list of all the fields and their uses:

1. Command to invoke the package manager.
2. Comma-separated list of commands supported by the package manager.
3. Whether or not to invoke the root command.
4. Command to use for root invocation (`sudo`, `doas`, etc.)
5. Comma-separated list of shortcuts for pak to accept
6. Comma-separated list of shortcut mappings (what each shortcut sends to the
  package manager). These do not necessarily need to be in the commands list.

Once you have made the config, just place it at `/etc/pak.cfg` and pak will
automatically use it.
