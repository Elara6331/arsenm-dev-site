---
title: "Installation"
draft: false
description: "Installing pak"
---
{{< appveyor-ci project="pak" projectID="e4yacqd78gkte8a0" >}}
{{< minio-s3 project="pak" >}}


## Using precompiled binary
Pak uses continuous integration to compile. You can find the binary by clicking the download badge above.

## Using the AUR
If you are running an arch-based linux distro, you can use the Arch User Repository
to install pak. First, make sure the `yay` AUR helper is installed, then run the following:

```bash
yay -S pak
```

## Building from source

### Downloading
Pak is hosted on my Gitea instance. If that is down, it is also mirrored on Gitlab.

{{< button-gitea color="green" project="pak" text="Pak" >}}
{{< button-gitlab color="OrangeRed" project="pak" text="Pak" >}}

To download pak, you can either use the download button on Gitea or Gitlab, or
you can use the git CLI

To clone pak using the CLI, run one of the following commands:
```bash
git clone https://gitea.arsenm.dev/Arsen6331/pak
OR
git clone https://gitlab.com/moussaelianarsen/pak
```

### Building

Pak is written in Go. This means go must be installed on your computer. Most
linux distros call the package that provides it either `go` or `golang`.

Once go is installed, you can check that it runs by running
```bash
go version
```
To compile pak, run
```bash
make
```
Then, you will need to figure out which package manager you have. Here is a list
of package managers with ready to use configs:

- apt
- aptitude
- brew
- yay (with wrapper)
- pacman (with wrapper)
- zypper
- snap

If your package manager is not in the list, you can make a config for it. Go to
the Configuration page for more information.

### Installing

If your package manager is in the list, use one of these:

- apt: `sudo make aptinstall`
- aptitude: `sudo make aptitude`
- brew: `sudo make brewinstall`
- yay: `sudo make yayinstall`
- pacman: `sudo make pacinstall`
- zypper: `sudo make zyppinstall`
- snap: `sudo make snapinstall`
- custom: `sudo make installbinonly`

Once the command completes, unless you're using a custom config, pak should be ready
and you can run the following to make sure it works:

```bash
pak
```

Go to the Configuration page for instructions on making a custom config, you **must**
have a config for pak to function.
