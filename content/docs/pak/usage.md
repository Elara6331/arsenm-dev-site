---
title: "Usage"
draft: false
description: "Using pak"
---
{{< appveyor-ci project="pak" projectID="e4yacqd78gkte8a0" >}}
{{< minio-s3 project="pak" >}}

Using pak is simple, just run `pak` and one of the commands from the config file.
Pak understands partial commands, so these commands will be identical:

```bash
pak in <package>
OR
pak inst <package>
OR
pak install <package>
```

The lack of `sudo` is intentional. Pak will not allow running from root by default
as it already invokes root internally. To bypass this, simply give pak the `-r` flag.

Using shortcuts in pak is just as simple as commands, just run `pak` and a shortcut,
like this:

```bash
pak rm <package>
```
