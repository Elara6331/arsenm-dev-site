---
title: "Configuration"
draft: false
description: "Configuring simpledash"
---

## Flags

Simpledash can be run using the simpledash binary directly, or for convenience, using `advmake run`.

If using the binary directly, the listen IP, port, etc. can be configured via flags.

This is the help screen of simpledash:

```text
Usage of ./simpledash:
  -a, --addr ip         Bind address for HTTP server (default 0.0.0.0)
  -c, --config string   TOML config file (default "simpledash.toml")
      --hash string     Generate new bcrypt password hash
  -p, --port int        Bind port for HTTP server (default 8080)
simpledash: help requested
```
The default address of simpledash is `0.0.0.0:8080` meaning any origin IP on any interface, port 8080.

The `--hash` option creates a suitable bcrypt password hash for use in the config, prints it, and exits.

## Config file

Simpledash is configured using a TOML configuration file (simpledash.toml by default). It contains the users, cards, etc.

An example file is provided in the simpledash repository. It contains examples of all the card types in simpledash.

This is the example:
```toml
title = "SimpleDash"
theme = "dark"
loginRequired = false
allowProxy = ["https://www.metaweather.com/", "https://ifconfig.co/json"]

[session]
  name = "simpledash-session"

[users]
  [[users._public_.card]]
    type = "weather"
    title = "Weather"
    data = {"woeid" = "2442047"}

  [[users._public_.card]]
    type = "api"
    title = "Server IP (API card example)"
    url = "https://ifconfig.co/json"
    data = {"format" = """
    <p class="subtitle">${data.ip}</p>
    Country: ${data.country} (${data.country_iso})
    Time zone: ${data.time_zone}
    """}

  [users.admin]
    passwordHash = "$2a$10$w00dzQ1PP6nwXLhuzV2pFOUU6m8bcZXtDX3UVxpOYq3fTSwVMqPge"
    showPublic = true

    [[users.admin.card]]
      type = "status"
      title = "Google"
      icon = "ion:logo-google"
      desc = "Google search engine. Status card example."
      url = "https://www.google.com"

    [[users.admin.card]]
      type = "simple"
      title = "Gmail"
      icon = "simple-icons:gmail"
      desc = "Gmail mail client. Simple card example"
      url = "https://mail.google.com/"

    [[users.admin.card]]
      type = "collection"
      title = "Programming"
      icon = "entypo:code"
      [users.admin.card.data]
        Godoc = {"url" = "https://pkg.go.dev", "target" = "newTab"}
        Ruby-Doc = {"url" = "https://ruby-doc.org/", "target" = "sameTab"}

    [[users.admin.card]]
      type = "collection"
      title = "Science"
      icon = "ic:outline-science"
      data = {"Google Scholar" = {"url" = "https://scholar.google.com/", "target" = "sameTab"}}
```

### Title

The title field sets the name of the website which will be used in all mentions including title tags and headers.

### Theme

The theme can either be dark or light. The dark theme was generated using darkreader.

### LoginRequired

The loginRequired field denotes whether login is required to view the dashboard. If `false`, public cards will be viewable without logging in.

### Session

The session section contains one field, `name`. This field is the name of the session cookie set in the browser upon visiting simpledash.

### Users

The `users` section contains all users and their associated cards. A user can be defined like so:

```toml
[users.admin]
    passwordHash = "$2a$10$w00dzQ1PP6nwXLhuzV2pFOUU6m8bcZXtDX3UVxpOYq3fTSwVMqPge"
    showPublic = true
```

The `passwordHash` field contains a hash as created by the `--hash` flag.

The `showPublic` field is a boolean denoting whether to show public cards as well when logged in.

### Cards

Cards reside under their respective user in the config file. A card can be defined like so:

```toml
[[users.admin.card]]
    type = "status"
    title = "Google"
    icon = "ion:logo-google"
    desc = "Google search engine. Status card example."
    url = "https://www.google.com"
```

The cards contain various fields, some required, some not:

- `type`: The type of the card. (required)
- `title`: The title of the card to be used in the header. (required)
- `icon`: The icon to be used in various places depending on the card. Icons can be anything from [iconify](https://iconify.design).
- `desc`: The description of the card's content.
- `url`: The URL of the card to be used for various purposes depending on the card.
- `data`: A dictionary containing any extra data not listed above.

Card types can be added simply by adding a file to `resources/templates/cards`. The name of the file will be used as the name of the card type