---
title: "Simpledash Docs"
draft: false
description: "Documentation for Simpledash, a simple and fast web dashboard"
menu:
    docs:
        parent: "docs"
---
