---
title: "Building"
draft: false
description: "Building simpledash"
---

## Building from source

### Prerequisites

Simpledash utilizes an SQLite database for session storage. That means that sqlite3 must be installed along with its development files to build simpledash. It also means cross-compilation is not as simple as setting some environment variables.

On Debian, the required packages are: `sqlite3` and `sqlite3-dev`.

On Arch, the required package is `sqlite`.

---

### Building

To build simpledash, first, clone the git repository of simpledash. Then, use a terminal to enter the cloned directory and run:

```shell
go build
```

---

### Building with Advmake

Simpledash can be built with another of my projects, `advmake`. Using this also builds the CSS framework used in this project, Bulma.

To build simpledash using advmake, simply run:
```shell
advmake
```
To run via advmake, run:
```shell
advmake run
```