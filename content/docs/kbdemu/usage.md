---
title: "Usage"
draft: false
description: "Using kbdemu"
---
{{< appveyor-ci project="kbdemu" projectID="km2f1wiy7enuh6il" >}}
{{< minio-s3 project="kbdemu" >}}

This page assumes you have already installed KbdEmu. If not, follow the installation instructions on the installation page:

{{< button text="Installation" bgcolor="#357edd" fgcolor="white" link="../installation" >}}

## Scripts

KbdEmu uses [scpt](https://gitea.arsenm.dev/Arsen6331/scpt) as its scripting language. The example script for kbdemu looks like this:

```
set testKey to "x"
keystroke $testKey with action "hold"
sleep "1s"
keystroke $testKey with action "release"
type "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
scroll 5 with direction "up"
click "right"
move-mouse [0, 0]
send-notification "Test"
beep
open-location "https://www.arsenm.dev/"
set showDetails to (display-dialog "Show details?" with type "yesno")
if $showDetails {
  display-dialog {"Color: " + (pixel-color [100, 100]) + ", Mouse: " + (str (mouse-position))} with title "Details"
}
print {"\n" + (user-choice "test" with items ["Hello", "World", 3.1415926535, $GOOS, $GOARCH, true, false, (numcpu)])}
log "Complete!"
```

By default, the `kbdemu` command will look for and execute a file called `kbdemu.scpt`, but that can be changed using the `--file` flag.

---

## Builtins

KbdEmu comes with some extra functions for automation

---

### `numcpu`

Returns the amount of available CPUs as a number.

Example:
```
print (numcpu)
```

---

### `sleep`

Sleeps for a duration as specified by an unnamed string argument formatted according to the specification of golang's `time.ParseDuration()`:

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://golang.org/pkg/time#ParseDuration" >}}

Examples:
```
sleep "1s"
sleep "1h2m"
```

---

### `display-dialog`

Displays a dialog window with the specified parameters.

Examples:
```
display-dialog "Test1"
display-dialog "Test2" with title "Title Test"
print (display-dialog "Test3" with title "Entry Test" with type "entry")
```

These are all the supported dialog types:

- info
- warning
- error
- entry
- yesno

Default dialog type is info.

---

### `send-notification`

Sends a notification according to the specified parameters

Examples:
```
send-notification "Test"
send-notification "Test" with title "Title"
send-notification "Test" with title "Title" with icon "test.png"
```

---

### `beep`

Creates a beep sound, if impossible, falls back to sending bell character.

Examples:
```
beep
beep "3s"
```

---

### `click`

Emulates a mouse click

Example:
```
click "right"
```

---

### `scroll`

Scrolls the specifed amount in the specified direction

Example:
```
scroll 5 with direction "up"
```

---

### `move-mouse`

Moves the cursor to the specified coordinates

Example:
```
move-mouse [100, 200]
```

---

### `keystroke`

Emulates a key event

Examples:
```
keystroke "y" with action "hold"
sleep "2s"
keystroke "y" with action "release"
keystroke "x"
```

Default action is `tap` which presses and releases the key.

---

### `type`

Types a string using the keyboard

Example:
```
type "Hello, World"
```

---

### `mouse-position`

Returns the current mouse position in the form of an array containing two number elements.

Example:
```
print (mouse-position) # [0 0]
```

---

### `pixel-color`

Returns a string containing the hex color of the given coordinates.

Example:
```
print (pixel-color [100, 100]) # ffffff
```

---

### `log`

Logs the provided message at the given level.

Examples:
```
log "Complete"
log "Error" with level "fatal"
```

The available levels are:

- info
- debug
- warn
- fatal

Default log level is info

---

### `user-choice`

Displays a user choice dialog window with provided items, returns selected item.

Example:
```
user-choice "Choose an option" with items ["Hello", "World", 1, 3.14159, 6.28318]
```

---

### `open-location`

Opens given URL in the default application set to open it.

Examples:
```
open-location "https://www.arsenm.dev"
open-location "/home"
```

---

## Variables

KbdEmu exposes some variables for use in scripts.

---

### `$GOOS`

The value from golang's `runtime.GOOS`

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://golang.org/pkg/runtime#GOOS" >}}

---

### `$GOARCH`

The value from golang's `runtime.GOARCH`

{{< button text="Godoc" bgcolor="#00ACD7" fgcolor="white" icon="cib:go" link="https://golang.org/pkg/runtime#GOARCH" >}}

---

### `$arguments`

Non-flag command line arguments provided to KbdEmu

Example:

Command:
```bash
kbdemu --file script.scpt "Hello, World"
```
Contents of `script.scpt`:
```
print $arguments[0] # Hello, World
```