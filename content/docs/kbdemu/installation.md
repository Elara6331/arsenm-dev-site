---
title: "Installation"
draft: false
description: "Installing kbdemu"
---
{{< appveyor-ci project="kbdemu" projectID="km2f1wiy7enuh6il" >}}
{{< minio-s3 project="kbdemu" >}}

## Using precompiled binary
KbdEmu uses continuous integration to compile. You can find the binary by
clicking the CI badge above, selecting "Ubuntu2004" for the linux build or
"macOS" for the macOS build, and then going to the artifacts tab.

## Building from source

### Downloading
KbdEmu is hosted in two places, Gitea and Gitlab. Either one can be used as
it is mirrored from Gitea to Gitlab

{{< button-gitea color="green" project="kbdemu" text="KbdEmu" >}}
{{< button-gitlab color="OrangeRed" project="kbdemu" text="KbdEmu" >}}

To download kbdemu, you can either use the download button on one of the above, or
you can use the git command

To clone kbdemu using the command, run one of the following commands:
```bash
git clone https://gitea.arsenm.dev/Arsen6331/kbdemu
OR
git clone https://gitlab.com/moussaelianarsen/kbdemu
```

Now, you will want to `cd` into the root of this repo before completing the rest
of these instructions

### Building

Since KbdEmu is written in go, you will need go installed in order to compile it.
Most linux distros call the package providing it either `go` or `golang`.

Once go is installed, you can check that it runs by running
```bash
go version
```
To compile KbdEmu, run the following commands:
```bash
go get github.com/go-vgo/robotgo
go get github.com/BurntSushi/toml
go get github.com/rs/zerolog/log
source .envrc
go build
```

### Installing

To install kbdemu, run the following command:

```bash
sudo install -Dm755 kbdemu /usr/bin
```

Once this command completes, to test whether kbdemu was installed properly, run
this command:

```bash
kbdemu
```

You should get an error warning you that kbdemu cannot find a TOML file. If you do,
kbdemu is properly installed.
